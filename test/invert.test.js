const invert = require('../invert.cjs');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const expectedResult = {'Bruce Wayne': 'name', 36: 'age', 'Gotham': 'location'}

test('Invert', () => {
    expect(invert(testObject)).toStrictEqual(expectedResult)
});