const defaults = require('../defaults.cjs');
const testObject = {flavor: "chocolate"};

test('Defaults', () => {
    expect(defaults(testObject, {flavor: "vanilla", sprinkles: "lots"})).toStrictEqual({flavor: "chocolate", sprinkles: "lots"})
});