const keys = require('../keys.cjs');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

test('Keys', () => {
    expect(keys(testObject)).toStrictEqual(Object.keys(testObject))
});