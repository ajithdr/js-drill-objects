const values = require('../values.cjs');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

test('Values', () => {
    expect(values(testObject)).toStrictEqual(Object.values(testObject))
});