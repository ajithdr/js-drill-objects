const { mapObject, cb } = require('../mapObject.cjs');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

test('Map Object', () => {
    expect(mapObject({start: 5, end: 12}, cb)).toStrictEqual({start: 10, end: 17})
});