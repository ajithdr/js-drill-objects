const pairs = require('../pairs.cjs');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

test('Pairs', () => {
    expect(pairs(testObject)).toStrictEqual(Object.entries(testObject))
});