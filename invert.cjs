const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function invert(obj) {
    const invertedObject = {};
    
    for(let key in obj) {
        const val = obj[key];
        invertedObject[val] = key;
    }
    return invertedObject;
}

// console.log(invert(testObject));
module.exports = invert;