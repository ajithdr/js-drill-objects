const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function values(obj) {
    const arrayOfValues = [];

    for(let key in obj) {
        arrayOfValues.push(obj[key]);
    }
    return arrayOfValues;
}

// console.log(values(testObject));
module.exports = values;