const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function keys(obj) {
    const arrayOfKeys = [];
    
    for(let key in obj) {
        arrayOfKeys.push(key);
    }
    return arrayOfKeys;
}

// console.log(keys(testObject));
module.exports = keys;