const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function cb(val, key) {
    return val + 5;
}

function mapObject(obj, cb) {

    for(let key in obj) {
        obj[key] = cb(obj[key], key);
    }

    return obj;
}

// console.log(mapObject(testObject, cb));
module.exports = mapObject;