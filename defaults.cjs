const testObject = { name: 'Bruce Wayne', age: 0, location: 'Gotham' };

function defaults(obj, defaultProps) {
    
    for(let key in defaultProps) {
        if(!obj.hasOwnProperty(key)) {
            obj[key] = defaultProps[key];
        }
    }
    return obj;
}

// console.log(defaults({flavor: "chocolate"}, {flavor: "vanilla", sprinkles: "lots", age: 13}));
module.exports = defaults;