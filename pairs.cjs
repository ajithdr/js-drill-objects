const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function pairs(obj) {
    const arrayOfPairs = [];

    for(let key in obj) {
        arrayOfPairs.push([key, obj[key]]);
    }
    return arrayOfPairs;
}

// console.log(pairs(testObject));
module.exports = pairs;